<?php
// src/Form/RemitaPaymentForm.php

namespace Drupal\commerce_remita\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_payment\PluginForm\PaymentGatewayFormBase;
use Drupal\Core\Plugin\PluginFormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class RemitaPaymentForm.
 */
class RemitaPaymentForm extends PaymentGatewayFormBase implements PluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $payment_gateway_plugin = $this->plugin;
    $configuration = $payment_gateway_plugin->getConfiguration();
    $order = $this->entity->getOrder();

    $form['#action'] = $configuration['api_url'];
    $form['#method'] = 'POST';

    // Add the necessary hidden fields for the Remita API.
    $form['merchantId'] = [
      '#type' => 'hidden',
      '#value' => $configuration['merchant_id'],
    ];
    $form['serviceTypeId'] = [
      '#type' => 'hidden',
      '#value' => $configuration['service_type_id'],
    ];
    $form['orderId'] = [
      '#type' => 'hidden',
      '#value' => $order->id(),
    ];
    $form['amount'] = [
      '#type' => 'hidden',
      '#value' => $order->getTotalPrice()->getNumber(),
    ];
    $form['apiKey'] = [
      '#type' => 'hidden',
      '#value' => $configuration['api_key'],
    ];
    $form['publicKey'] = [
      '#type' => 'hidden',
      '#value' => $configuration['public_key'],
