<?php
// src/Plugin/Commerce/PaymentGateway/RemitaPayment.php

namespace Drupal\commerce_remita\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the Remita payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "remita_payment",
 *   label = "Remita",
 *   display_label = "Remita",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_remita\Form\RemitaPaymentForm",
 *   },
 *   payment_method_types = {"credit_card"},
 * )
 */
class RemitaPayment extends OffsitePaymentGatewayBase implements OffsitePaymentGatewayInterface {

/**
 * {@inheritdoc}
 */
public function defaultConfiguration() {
  return [
    'merchant_id' => '',
    'service_type_id' => '',
    'api_key' => '',
    'public_key' => '',
    'secret_key' => '',
    'api_url' => 'https://remitademo.net/remita/exapp/api/v1/send/api',
    'payment_method_types' => ['credit_card'], // Set the payment method types.
  ] + parent::defaultConfiguration();
}

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();

    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant ID'),
      '#default_value' => $configuration['merchant_id'],
      '#required' => TRUE,
    ];
    $form['service_type_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Service Type ID'),
      '#default_value' => $configuration['service_type_id'],
      '#required' => TRUE,
    ];
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $configuration['api_key'],
      '#required' => TRUE,
    ];
    $form['public_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Public Key'),
      '#default_value' => $configuration['public_key'],
      '#required' => TRUE,
      // Set a higher maxlength to accommodate longer keys.
      '#maxlength' => 512,
    ];
    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret Key'),
      '#default_value' => $configuration['secret_key'],
      '#required' => TRUE,
    ];
    $form['api_url'] = [
      '#type' => 'url',
      '#title' => $this->t('API URL'),
      '#default_value' => $configuration['api_url'],
      '#required' => TRUE,
    ];

    return $form;
  }

/**
 * {@inheritdoc}
 */
public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
  parent::submitConfigurationForm($form, $form_state);
  
  // Retrieve the values from the form state.
  $values = $form_state->getValues();

  // Ensure configuration values are initialized and not null before assigning.
  $configuration = $this->getConfiguration();
  $configuration['merchant_id'] = !empty($values['merchant_id']) ? $values['merchant_id'] : '';
  $configuration['service_type_id'] = !empty($values['service_type_id']) ? $values['service_type_id'] : '';
  $configuration['api_key'] = !empty($values['api_key']) ? $values['api_key'] : '';
  $configuration['public_key'] = !empty($values['public_key']) ? $values['public_key'] : '';
  $configuration['secret_key'] = !empty($values['secret_key']) ? $values['secret_key'] : '';
  $configuration['api_url'] = !empty($values['api_url']) ? $values['api_url'] : '';

  // Set the updated configuration.
  $this->setConfiguration($configuration);
}


  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');

    // Retrieve the transaction status from the request.
    $status = $request->get('status');
    $transactionId = $request->get('transactionId');

    if ($status == 'success') {
      // Mark the payment as completed.
      $payment = $payment_storage->create([
        'state' => 'completed',
        'amount' => $order->getTotalPrice(),
        'payment_gateway' => $this->parentEntity->id(),
        'order_id' => $order->id(),
        'remote_id' => $transactionId,
        'remote_state' => $status,
      ]);
      $payment->save();
      $order->set('state', 'completed');
      $order->save();
      \Drupal::logger('commerce_remita')->notice('Payment completed for orderId: @orderId', ['@orderId' => $order->id()]);
    } else {
      // Handle failed payment.
      \Drupal::logger('commerce_remita')->error('Payment failed for orderId: @orderId with status: @status', ['@orderId' => $order->id(), '@status' => $status]);
      drupal_set_message(t('Payment failed. Please try again.'), 'error');
      $order->set('state', 'canceled');
      $order->save();
    }
  }
}
