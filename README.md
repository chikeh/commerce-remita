# Commerce Remita

Commerce Remita adds the [Remita](https://remita.net/) payment processor as a payment method for [Drupal Commerce](https://www.drupal.org/project/commerce).

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-modules).

## Configuration

Module is not completed yet. Don't use yet.